package br.com.mateusmeyer.pag_proj.bill_person_stake;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.mateusmeyer.pag_proj.SpringTest;
import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.bill.BillItemCategory;
import br.com.mateusmeyer.pag_proj.bill.BillItemService;
import br.com.mateusmeyer.pag_proj.person.Person;
import static br.com.mateusmeyer.pag_proj.bill.SharedBillTestData.*;

@SpringTest
public class ProportionalBillPersonStakeStrategyTest {
    @Autowired
    BillItemService service;

    static List<SimpleEntry<BigDecimal, Person>> examplePersonsAndStakes() {
        List<Person> persons = createTwoExamplePersons();

        return Arrays.asList(
            new SimpleEntry<>(new BigDecimal("0.8400"), persons.get(0)), // 84% of 50 (40 + 2) / 50
            new SimpleEntry<>(new BigDecimal("0.1600"), persons.get(1))  // 16% of 50 (8) / 50
        );
    }

    @ParameterizedTest
    @DisplayName("Tests if stake is generated correctly for each person")
    @MethodSource("examplePersonsAndStakes")
    void testStakeIsGeneratedCorrectlyForPerson(SimpleEntry<BigDecimal, Person> stakeAndPerson) {
        Bill bill = new Bill();
        List<Person> persons = createTwoExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();
        List<BillItem> items = createExampleBillItems(
            persons.get(0),
            persons.get(1),
            categories.get(0),
            categories.get(1),
            categories.get(2)
        );
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        List<BillItem> itemsOfAnyPerson = service.getBillItemsWithAnyPerson(items);
        List<BillItem> itemsOfCurrentPerson = service.getBillItemsWithPerson(items, stakeAndPerson.getValue());
        BigDecimal rawTotalPrice = service.sumBillItems(items);
        BigDecimal itemsOfAnyPersonPrice = service.sumBillItems(itemsOfAnyPerson);
   
        ProportionalBillPersonStakeStrategy stakeStrategy = new ProportionalBillPersonStakeStrategy();
        BigDecimal stake = stakeStrategy.getStake(
            bill,
            stakeAndPerson.getValue(),
            rawTotalPrice,
            itemsOfAnyPerson,
            itemsOfAnyPersonPrice,
            itemsOfCurrentPerson,
            persons
        );

        assertEquals(stakeAndPerson.getKey(), stake);
    }

    static List<SimpleEntry<BigDecimal, Person>> example2PersonsAndStakes() {
        List<Person> persons = createThreeExamplePersons();

        return Arrays.asList(
            new SimpleEntry<>(new BigDecimal("0.4000"), persons.get(0)), // 40% of 50 (20) / 50
            new SimpleEntry<>(new BigDecimal("0.1200"), persons.get(1)),  // 48% of 50 (24) / 50
            new SimpleEntry<>(new BigDecimal("0.4800"), persons.get(2))  // 12% of 50 (6) / 50
        );
    }

    @ParameterizedTest
    @DisplayName("Tests if stake is generated correctly for each person, with items shared with more than one person")
    @MethodSource("example2PersonsAndStakes")
    void testStakeIsGeneratedCorrectlyForPersonSharedItems(SimpleEntry<BigDecimal, Person> stakeAndPerson) {
        Bill bill = new Bill();
        List<Person> persons = createThreeExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();
        List<BillItem> items = createExampleMultiPersonBillItems(
            persons.get(0),
            persons.get(1),
            persons.get(2),
            categories.get(0),
            categories.get(1),
            categories.get(2)
        );
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        List<BillItem> itemsOfAnyPerson = service.getBillItemsWithAnyPerson(items);
        List<BillItem> itemsOfCurrentPerson = service.getBillItemsWithPerson(items, stakeAndPerson.getValue());
        BigDecimal rawTotalPrice = service.sumBillItems(items);
        BigDecimal itemsOfAnyPersonPrice = service.sumBillItems(itemsOfAnyPerson);
   
        ProportionalBillPersonStakeStrategy stakeStrategy = new ProportionalBillPersonStakeStrategy();
        BigDecimal stake = stakeStrategy.getStake(
            bill,
            stakeAndPerson.getValue(),
            rawTotalPrice,
            itemsOfAnyPerson,
            itemsOfAnyPersonPrice,
            itemsOfCurrentPerson,
            persons
        );

        assertEquals(stakeAndPerson.getKey(), stake);
    }
}
