package br.com.mateusmeyer.pag_proj.bill;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.mateusmeyer.pag_proj.SpringTest;
import br.com.mateusmeyer.pag_proj.person.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;

@SpringTest
@MockBean(BillItemCategoryRepository.class)
public class BillItemServiceTest {
    @Autowired
    BillItemService service;

    @Autowired
    BillItemCategoryRepository itemCategoryRepository;

    @Test
    @DisplayName("Tests if sumBillItems works")
    void testSumBillItemsWorks() {
        BillItemCategory productCategory = new BillItemCategory("product");
        BillItemCategory discountCategory = new BillItemCategory("discount", true);

        Person personOne = new Person("Person One");
        Person personTwo = new Person("Person Two");

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(personOne, personTwo), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("13.01"), Arrays.asList(personOne), discountCategory)
        );

        BigDecimal sumValue = service.sumBillItems(items);

        assertEquals(BillUtils.toTwoDigitsSafe("16.66"), sumValue);
    }

    @Test
    @DisplayName("Tests if getBillItemsWithPerson works")
    void testGetBillItemsWithPersonWorks() {
        BillItemCategory productCategory = new BillItemCategory("product");

        Person personOne = new Person("Person One");
        Person personTwo = new Person("Person Two");

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("13.01"), Arrays.asList(personOne, personTwo), productCategory)
        );

        List<BillItem> filteredItems = service.getBillItemsWithPerson(items, personTwo);

        assertEquals(Arrays.asList(items.get(2)), filteredItems);
    }

    @Test
    @DisplayName("Tests if getBillItemsWithAnyPerson works")
    void testGetBillItemsWithAnyPersonWorks() {
        BillItemCategory productCategory = new BillItemCategory("product");

        Person personOne = new Person("Person One");

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("13.01"), Arrays.asList(), productCategory)
        );

        List<BillItem> filteredItems = service.getBillItemsWithAnyPerson(items);

        assertEquals(Arrays.asList(items.get(1)), filteredItems);
    }

    @Test
    @DisplayName("Tests if getBillItemsWithoutPerson works")
    void testGetBillItemsWithoutPersonWorks() {
        BillItemCategory productCategory = new BillItemCategory("product");

        Person personOne = new Person("Person One");
        Person personTwo = new Person("Person Two");

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("13.01"), Arrays.asList(personOne, personTwo), productCategory)
        );

        List<BillItem> filteredItems = service.getBillItemsWithoutPerson(items);

        assertEquals(Arrays.asList(items.get(1)), filteredItems);
    }

    @Test
    @DisplayName("Tests if useOnlyValidCategories repository exists() check is made")
    void testUseOnlyValidCategoriesRepositoryExistsCheckIsMade() {
        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2")
        );
        List<BillItemCategory> categories = Arrays.asList(
            new BillItemCategory("delivery", uuids.get(0)),
            new BillItemCategory("payment", uuids.get(1))
        );

        when(itemCategoryRepository.existsByUniqueId(isA(UUID.class)))
            .thenReturn(true);

        service.validateCategories(categories);

        // Asserting existsByUniqueId() is called for every passed member
        ArgumentCaptor<UUID> captor = ArgumentCaptor.forClass(UUID.class);
        verify(itemCategoryRepository, times(2))
            .existsByUniqueId(captor.capture());
        
        assertEquals(uuids, captor.getAllValues());
    }

    @Test
    @DisplayName("Tests if useOnlyValidCategories repository throws on non-existent category")
    void testUseOnlyValidCategoriesRepositoryThrowOnNonExistentCategory() {
        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2")
        );
        List<BillItemCategory> categories = Arrays.asList(
            new BillItemCategory("delivery", uuids.get(0)),
            new BillItemCategory("payment", uuids.get(1))
        );

        when(itemCategoryRepository.existsByUniqueId(isA(UUID.class)))
            .thenReturn(true);
        when(itemCategoryRepository.existsByUniqueId(eq(uuids.get(1))))
            .thenReturn(false);

        assertThrows(IllegalArgumentException.class, () -> {
            service.validateCategories(categories);
        });
    }

    @Test
    @DisplayName("Tests if getPersonOfBillItems returns the correct person")
    void testGetPersonOfBillItems() {
        BillItemCategory productCategory = new BillItemCategory("product");

        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2")
        );

        Person personOne = new Person("Person One", uuids.get(0));
        Person personTwo = new Person("Person Two", uuids.get(1));

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("-13.01"), Arrays.asList(personOne, personTwo), productCategory)
        );

        Optional<Person> person = service.getPersonOfBillItems(items, uuids.get(1));

        assertEquals(uuids.get(1), person.get().getUniqueId());
    }

    @Test
    @DisplayName("Tests if getPersonOfBillItems returns no person on nonexistent UUID")
    void testGetPersonOfBillItemsReturnsNull() {
        BillItemCategory productCategory = new BillItemCategory("product");

        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2"),
            UUID.fromString("3e42b143-aa7e-42e6-bab2-8f1ce6361a4f")
        );

        Person personOne = new Person("Person One", uuids.get(0));
        Person personTwo = new Person("Person Two", uuids.get(1));

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(personOne), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("8.35"), Arrays.asList(), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("-13.01"), Arrays.asList(personOne, personTwo), productCategory)
        );

        Optional<Person> person = service.getPersonOfBillItems(items, uuids.get(2));

        assertEquals(false, person.isPresent());
    }

    static class BillPercentageTuple {
        public Integer personIndex;
        public BigDecimal personStake;
        public BigDecimal expectedSum;

        public BillPercentageTuple(Integer personIndex, BigDecimal personStake,  BigDecimal expectedSum) {
            this.personIndex = personIndex;
            this.personStake = personStake;
            this.expectedSum = expectedSum;
        }
    }

    static List<BillPercentageTuple> billPercentageStakes() {
        return Arrays.asList(
            new BillPercentageTuple(0, new BigDecimal("0.8400"), new BigDecimal("3.53")),
            new BillPercentageTuple(1, new BigDecimal("0.1600"), new BigDecimal("0.13"))
        );
    }

    @ParameterizedTest
    @MethodSource("billPercentageStakes")
    void testApplyBillPercentagesWork(BillPercentageTuple entry) {
        BillItemCategory productCategory = new BillItemCategory("product", false, true);
        BillItemCategory tipCategory = new BillItemCategory("tip", false);

        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2"),
            UUID.fromString("3e42b143-aa7e-42e6-bab2-8f1ce6361a4f")
        );

        List<Person> persons = Arrays.asList(
            new Person("Person One", uuids.get(0)),
            new Person("Person Two", uuids.get(1))
        );

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("40"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("2"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("8"), Arrays.asList(persons.get(1)), productCategory),
            new BillItem("10% Tip", BillUtils.toTwoDigitsSafe("10"), Arrays.asList(), tipCategory, true)
        );

        Person expectedPerson = persons.get(entry.personIndex);
        List<BillItem> personItems = service.getBillItemsWithPerson(items, expectedPerson);
        BigDecimal personTotal = service.sumBillItems(personItems);
        BigDecimal value = service.applyBillPercentages(items, expectedPerson, personTotal, entry.personStake);

        assertEquals(entry.expectedSum, value);
    }

    static List<BillPercentageTuple> billPercentageSinglePersonStakes() {
        return Arrays.asList(
            new BillPercentageTuple(0, new BigDecimal("0.8400"), new BigDecimal("4.20")),
            new BillPercentageTuple(1, new BigDecimal("0.1600"), new BigDecimal("0.00"))
        );
    }

    @ParameterizedTest
    @MethodSource("billPercentageSinglePersonStakes")
    void testApplyBillPercentagesWorkInPersonSpecific(BillPercentageTuple entry) {
        BillItemCategory productCategory = new BillItemCategory("product", false, true);
        BillItemCategory tipCategory = new BillItemCategory("tip", false);

        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2"),
            UUID.fromString("3e42b143-aa7e-42e6-bab2-8f1ce6361a4f")
        );

        List<Person> persons = Arrays.asList(
            new Person("Person One", uuids.get(0)),
            new Person("Person Two", uuids.get(1))
        );

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("40"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("2"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("8"), Arrays.asList(persons.get(1)), productCategory),
            new BillItem("10% Tip", BillUtils.toTwoDigitsSafe("10"), Arrays.asList(persons.get(0)), tipCategory, true)
        );

        Person expectedPerson = persons.get(entry.personIndex);
        List<BillItem> personItems = service.getBillItemsWithPerson(items, expectedPerson);
        BigDecimal personTotal = service.sumBillItems(personItems);
        BigDecimal value = service.applyBillPercentages(items, expectedPerson, personTotal, entry.personStake);

        assertEquals(entry.expectedSum, value);
    }

    static List<BillPercentageTuple> billPercentageMultiPersonStakes() {
        return Arrays.asList(
            new BillPercentageTuple(0, new BigDecimal("0.8400"), new BigDecimal("2.10")),
            new BillPercentageTuple(1, new BigDecimal("0.1600"), new BigDecimal("0.40"))
        );
    }

    @ParameterizedTest
    @MethodSource("billPercentageMultiPersonStakes")
    void testApplyBillPercentagesAreEquallyDividedOnMultiplePersons(BillPercentageTuple entry) {
        BillItemCategory productCategory = new BillItemCategory("product", false, true);
        BillItemCategory tipCategory = new BillItemCategory("tip", false);

        List<UUID> uuids = Arrays.asList(
            UUID.fromString("17521cbd-f96f-4223-ac3e-122176ee5335"),
            UUID.fromString("aa350210-14f9-45bf-bb8c-22f8ffe6f3e2"),
            UUID.fromString("3e42b143-aa7e-42e6-bab2-8f1ce6361a4f")
        );

        List<Person> persons = Arrays.asList(
            new Person("Person One", uuids.get(0)),
            new Person("Person Two", uuids.get(1))
        );

        List<BillItem> items = Arrays.asList(
            new BillItem("Item One", BillUtils.toTwoDigitsSafe("40"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Two", BillUtils.toTwoDigitsSafe("2"), Arrays.asList(persons.get(0)), productCategory),
            new BillItem("Item Three", BillUtils.toTwoDigitsSafe("8"), Arrays.asList(persons.get(1)), productCategory),
            new BillItem("10% Tip", BillUtils.toTwoDigitsSafe("10"), Arrays.asList(persons.get(0), persons.get(1)), tipCategory, true)
        );

        Person expectedPerson = persons.get(entry.personIndex);
        List<BillItem> personItems = service.getBillItemsWithPerson(items, expectedPerson);
        BigDecimal personTotal = service.sumBillItems(personItems);
        BigDecimal value = service.applyBillPercentages(items, expectedPerson, personTotal, entry.personStake);

        assertEquals(entry.expectedSum, value);
    }

    @Test
    void testValidateBillItemVerifiesCategoryHasPerson() {
        BillItemCategory productCategory = new BillItemCategory("product", false, true);
        BillItem item = new BillItem("Item One", BillUtils.toTwoDigitsSafe("21.32"), Arrays.asList(), productCategory);

        assertThrows(IllegalArgumentException.class, () -> {
            service.validateBillItem(item);
        });
    }
}
