package br.com.mateusmeyer.pag_proj.bill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import br.com.mateusmeyer.pag_proj.person.Person;

public class SharedBillTestData {
    public static List<Person> createTwoExamplePersons() {
        Person firstPerson = new Person("Person One", UUID.fromString("8f4e87d9-c18c-44f3-81ba-14c476a59888"));
        firstPerson.setId(1L);
        Person secondPerson = new Person("Person Two", UUID.fromString("8f71b9a7-8eef-4de2-824b-19c074f867b8"));
        firstPerson.setId(2L);

        return Arrays.asList(firstPerson, secondPerson);
    }

    public static List<Person> createThreeExamplePersons() {
        Person thirdPerson = new Person("Person Three", UUID.fromString("6983a579-c61a-4633-941f-31e3d7643d8e"));
        thirdPerson.setId(3L);

        List<Person> persons = new ArrayList<>(createTwoExamplePersons());
        persons.add(thirdPerson);
        return persons;
    }

    public static List<BillItem> createExampleBillItems(
        Person firstPerson,
        Person secondPerson, 
        BillItemCategory productCategory,
        BillItemCategory discountCategory,
        BillItemCategory deliveryCategory
    ) {
        List<BillItem> items = new ArrayList<>();
        items.add(new BillItem("Hamburger", BillUtils.toTwoDigitsSafe("40.0"), Arrays.asList(firstPerson), productCategory));
        items.add(new BillItem("Dessert", BillUtils.toTwoDigitsSafe("2.0"), Arrays.asList(firstPerson), productCategory));
        items.add(new BillItem("Sandwich", BillUtils.toTwoDigitsSafe("8.0"), Arrays.asList(secondPerson), productCategory));
        items.add(new BillItem("Discount Coupon", BillUtils.toTwoDigitsSafe("20.0"), Arrays.asList(), discountCategory));
        items.add(new BillItem("Nearby Delivery", BillUtils.toTwoDigitsSafe("8.0"), Arrays.asList(), deliveryCategory));

        return items;
    }

    public static List<BillItem> createExampleThreePersonBillItems(
        Person firstPerson,
        Person secondPerson, 
        Person thirdPerson, 
        BillItemCategory productCategory,
        BillItemCategory discountCategory,
        BillItemCategory deliveryCategory
    ) {
        List<BillItem> items = new ArrayList<>();
        items.add(new BillItem("Hamburger", BillUtils.toTwoDigitsSafe("34.0"), Arrays.asList(firstPerson), productCategory));
        items.add(new BillItem("Dessert", BillUtils.toTwoDigitsSafe("34.0"), Arrays.asList(secondPerson), productCategory));
        items.add(new BillItem("Sandwich", BillUtils.toTwoDigitsSafe("34.0"), Arrays.asList(thirdPerson), productCategory));
        items.add(new BillItem("Discount Coupon", BillUtils.toTwoDigitsSafe("2.0"), Arrays.asList(), discountCategory));

        return items;
    }

    public static List<BillItem> createExampleMultiPersonBillItems(
        Person firstPerson,
        Person secondPerson, 
        Person thirdPerson, 
        BillItemCategory productCategory,
        BillItemCategory discountCategory,
        BillItemCategory deliveryCategory
    ) {
        List<BillItem> items = new ArrayList<>();
        items.add(new BillItem("Hamburger", BillUtils.toTwoDigitsSafe("40.0"), Arrays.asList(firstPerson, thirdPerson), productCategory));
        items.add(new BillItem("Dessert", BillUtils.toTwoDigitsSafe("2.0"), Arrays.asList(secondPerson), productCategory));
        items.add(new BillItem("Sandwich", BillUtils.toTwoDigitsSafe("8.0"), Arrays.asList(secondPerson, thirdPerson), productCategory));
        items.add(new BillItem("Discount Coupon", BillUtils.toTwoDigitsSafe("20.0"), Arrays.asList(), discountCategory));
        items.add(new BillItem("Nearby Delivery", BillUtils.toTwoDigitsSafe("8.0"), Arrays.asList(), deliveryCategory));

        return items;
    }

    public static List<BillItemCategory> createExampleBillItemCategories() {
        List<BillItemCategory> items = new ArrayList<>();
        items.add(new BillItemCategory("product", false, true));
        items.add(new BillItemCategory("discount", true, false));
        items.add(new BillItemCategory("delivery", false, false));

        return items;
    }
    
}
