package br.com.mateusmeyer.pag_proj.person;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.mateusmeyer.pag_proj.SpringTest;

@SpringTest
@MockBean(PersonRepository.class)
public class PersonServiceTest {

    @Autowired
    private PersonService service;

    @Autowired
    private PersonRepository repository;

    @Test
    void testCreatePersonIfNeededWorks() {
        List<Person> persons = Arrays.asList(
            new Person("Person One"),
            new Person("Person Two"),
            new Person("Person Three")
        );

        service.createPersonsIfNeeded(persons);

        ArgumentCaptor<Person> values = ArgumentCaptor.forClass(Person.class);
        verify(repository, times(3)).save(values.capture());

        assertEquals(persons, values.getAllValues());
    }
}
