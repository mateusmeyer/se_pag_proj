package br.com.mateusmeyer.pag_proj.bill_payment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import br.com.mateusmeyer.pag_proj.bill.BillUtils;

public class PicPayLinkBillPaymentProviderTest {
    @Test
    void testFields() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();

        Map<String, BillPaymentProviderField> fields = provider.getFields(null);

        assertEquals(Stream.of("target_account").collect(Collectors.toSet()), fields.keySet());
    }

    @Test
    void testValidatesTargetAccountMissing() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();
        Map<String, Object> inputFields = new HashMap<>();

        Map<String, Map<String, String>> errors = provider.validateFields(null, inputFields);

        assertEquals(Stream.of("missing_value").collect(Collectors.toSet()), errors.get("target_account").keySet());
    }

    @Test
    void testValidatesTargetAccountEmpty() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();
        Map<String, Object> inputFields = new HashMap<>();
        inputFields.put("target_account", "  ");

        Map<String, Map<String, String>> errors = provider.validateFields(null, inputFields);

        assertEquals(Stream.of("missing_value").collect(Collectors.toSet()), errors.get("target_account").keySet());
    }

    @Test
    void testValidatesTargetAccountNonString() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();
        Map<String, Object> inputFields = new HashMap<>();
        inputFields.put("target_account", 32613);

        Map<String, Map<String, String>> errors = provider.validateFields(null, inputFields);

        assertEquals(Stream.of("must_be_string").collect(Collectors.toSet()), errors.get("target_account").keySet());
    }

    @Test
    void testValidatesTargetAccountPresent() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();
        Map<String, Object> inputFields = new HashMap<>();
        inputFields.put("target_account", "test.person");

        Map<String, Map<String, String>> errors = provider.validateFields(null, inputFields);

        assertEquals(0, errors.size());
    }

    @Test
    void testGeneratesCorrectPaymentLink() {
        BillPaymentProvider provider = new PicPayLinkBillPaymentProvider();
        Map<String, Object> inputFields = new HashMap<>();
        inputFields.put("target_account", "test.person");

        BillPayment payment = new BillPayment();
        payment.setBillTotal(BillUtils.toTwoDigitsSafe("32.01"));
        payment.setPersonTotal(BillUtils.toTwoDigitsSafe("16.67"));

        String generatedUrl = provider.generatePaymentLink(payment, inputFields);

        assertEquals("https://picpay.me/test.person/16.67", generatedUrl);
    }
}
