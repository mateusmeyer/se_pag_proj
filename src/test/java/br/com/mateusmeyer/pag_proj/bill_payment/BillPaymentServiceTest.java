package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.mateusmeyer.pag_proj.SpringTest;
import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.bill.BillItemCategory;
import br.com.mateusmeyer.pag_proj.bill.BillItemCategoryRepository;
import br.com.mateusmeyer.pag_proj.bill.BillUtils;
import br.com.mateusmeyer.pag_proj.person.Person;
import br.com.mateusmeyer.pag_proj.person.PersonRepository;

import static br.com.mateusmeyer.pag_proj.bill.SharedBillTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringTest
@MockBean(BillItemCategoryRepository.class)
@MockBean(PersonRepository.class)
@MockBean(BillPaymentRepository.class)
@MockBean(BillPaymentConfiguration.class)
public class BillPaymentServiceTest {
    @Autowired
    BillPaymentService service;

    @Autowired
    BillItemCategoryRepository itemCategoryRepository;

    @Autowired
    PersonRepository personRepository;

    @Test
    void testGeneratePaymentsForStakeWorks() {
        Bill bill = new Bill();
        List<Person> persons = createTwoExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();
        List<BillItem> items = createExampleBillItems(
            persons.get(0),
            persons.get(1),
            categories.get(0),
            categories.get(1),
            categories.get(2)
        );
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        List<BillPayment> payments = service.generatePaymentsForStake(
            bill,
            persons.get(0),
            BillUtils.toTwoDigitsSafe("50.0"),
            BillUtils.toTwoDigitsSafe("0.17"),
            BillUtils.toTwoDigitsSafe("0.00")
        );

        assertEquals(BillUtils.toTwoDigitsSafe("50.0"), payments.get(0).getBillTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("0.17"), payments.get(0).getStake());
        assertEquals(BillUtils.toTwoDigitsSafe("8.5"), payments.get(0).getPersonTotal());
    }

    @Test
    void testDoBillDivisionWorksSimpleCase() {
        Bill bill = new Bill();
        List<Person> persons = createTwoExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();
        List<BillItem> items = createExampleBillItems(
            persons.get(0),
            persons.get(1),
            categories.get(0),
            categories.get(1),
            categories.get(2)
        );
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        when(itemCategoryRepository.existsByUniqueId(any()))
            .thenReturn(true);
        when(personRepository.save(any()))
            .thenAnswer(x -> x.getArgument(0));

        List<BillPayment> payments = service.doAndSaveBillDivision(bill, "proportional", null);

        assertEquals(2, payments.size());
        assertEquals(BillUtils.toTwoDigitsSafe("31.92"), payments.get(0).getPersonTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("6.08"), payments.get(1).getPersonTotal());
    }

    @Test
    void testDoBillDivisionReturnsEmptyOnZeroValueItems() {
        Bill bill = new Bill();
        List<Person> persons = createTwoExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();

        List<BillItem> items = new ArrayList<>();
        items.add(new BillItem("Hamburger", BillUtils.toTwoDigitsSafe("0.0"), Arrays.asList(persons.get(0)), categories.get(0)));
        items.add(new BillItem("Dessert", BillUtils.toTwoDigitsSafe("0.0"), Arrays.asList(persons.get(0)), categories.get(0)));

        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        when(itemCategoryRepository.existsByUniqueId(any()))
            .thenReturn(true);
        when(personRepository.save(any()))
            .thenAnswer(x -> x.getArgument(0));

        List<BillPayment> payments = service.doAndSaveBillDivision(bill, "proportional", null);

        assertEquals(0, payments.size());
    }

    @Test
    @DisplayName("Test doBillDivision works with extraneous/non-exact value")
    void testDoBillDivisionWorksExtraneous() {
        Bill bill = new Bill();
        List<Person> persons = createThreeExamplePersons();
        List<BillItemCategory> categories = createExampleBillItemCategories();
        List<BillItem> items = createExampleThreePersonBillItems(
            persons.get(0),
            persons.get(1),
            persons.get(2),
            categories.get(0),
            categories.get(1),
            categories.get(2)
        );
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        when(itemCategoryRepository.existsByUniqueId(any()))
            .thenReturn(true);
        when(personRepository.save(any()))
            .thenAnswer(x -> x.getArgument(0));

        List<BillPayment> payments = service.doAndSaveBillDivision(bill, "proportional", persons.get(1));

        assertEquals(3, payments.size());
        assertEquals(BillUtils.toTwoDigitsSafe("33.33"), payments.get(0).getPersonTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("33.34"), payments.get(1).getPersonTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("33.33"), payments.get(2).getPersonTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("0.01"), payments.get(1).getExtraneousPayment());
    }

    @Test
    @DisplayName("Test doBillDivision works with percentage")
    void testDoBillDivisionWorksWithPercentage() {
        Bill bill = new Bill();
        List<Person> persons = createThreeExamplePersons();

        BillItemCategory productCategory = new BillItemCategory("product", false, true);
        BillItemCategory tipCategory = new BillItemCategory("tip", false, false);
        
        List<BillItem> items = new ArrayList<>();
        items.add(new BillItem("Hamburger", BillUtils.toTwoDigitsSafe("40.0"), Arrays.asList(persons.get(0)), productCategory));
        items.add(new BillItem("Dessert", BillUtils.toTwoDigitsSafe("2.0"), Arrays.asList(persons.get(0)), productCategory));
        items.add(new BillItem("Sandwich", BillUtils.toTwoDigitsSafe("8.0"), Arrays.asList(persons.get(1)), productCategory));
        items.add(new BillItem("Tip", BillUtils.toTwoDigitsSafe("10.0"), Arrays.asList(), tipCategory, true));
        bill.setTitle("Delivery Invoice");
        bill.setItems(items);

        when(itemCategoryRepository.existsByUniqueId(any()))
            .thenReturn(true);
        when(personRepository.save(any()))
            .thenAnswer(x -> x.getArgument(0));

        List<BillPayment> payments = service.doAndSaveBillDivision(bill, "proportional", persons.get(1));

        assertEquals(2, payments.size());
        assertEquals(BillUtils.toTwoDigitsSafe("45.53"), payments.get(0).getPersonTotal());
        assertEquals(BillUtils.toTwoDigitsSafe("8.13"), payments.get(1).getPersonTotal());
    }

    @Test
    void testGetProviderForPaymentWorks() {
        BillPaymentProvider picpayProvider = new PicPayLinkBillPaymentProvider();
        BillPaymentProvider testProvider = new BillPaymentProvider() {
            @Override
            public String getName() {return "test-provider";}

            @Override
            public String getTitle() {return "Test Provider";}

            @Override
            public boolean canHandle(BillPayment payment) {return false;}

            @Override
            public Map<String, BillPaymentProviderField> getFields(BillPayment payment) {return null;}

            @Override
            public Map<String, Map<String, String>> validateFields(BillPayment payment, Map<String, Object> fields) {return null;}

            @Override
            public String generatePaymentLink(BillPayment payment, Map<String, Object> fields) {return null;}
        };

        service.addProvider(picpayProvider);
        service.addProvider(testProvider);

        BillPayment payment = new BillPayment();
        Map<String, BillPaymentProviderItem> providers = service.getProvidersForPayment(payment);

        assertEquals(Stream.of("picpay_link").collect(Collectors.toSet()), providers.keySet()); 
    }
}
