package br.com.mateusmeyer.pag_proj.bill_payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.bill.BillItemService;
import br.com.mateusmeyer.pag_proj.bill.BillUtils;
import br.com.mateusmeyer.pag_proj.bill_person_stake.BillPersonStakeStrategy;
import br.com.mateusmeyer.pag_proj.bill_person_stake.EqualityBillPersonStakeStrategy;
import br.com.mateusmeyer.pag_proj.bill_person_stake.ProportionalBillPersonStakeStrategy;
import br.com.mateusmeyer.pag_proj.person.Person;
import br.com.mateusmeyer.pag_proj.person.PersonService;

@Service
public class BillPaymentService {
    @Autowired
    private PersonService personService;

    @Autowired
    private BillItemService billItemService;

    @Autowired
    private BillPaymentRepository repository;

    public Optional<BillPayment> getByUniqueId(UUID uniqueId) {
        return repository.findByUniqueId(uniqueId);
    }

    /**
     * Generates the bill division and payment
     * @param bill The bill.
     * @param divisionMode The division mode (see {@link #getStakeStrategy(Bill, String)})
     * @param preferredExtraneousPayer
     * @return
     */
    @Transactional
    public List<BillPayment> doAndSaveBillDivision(
        Bill bill,
        String divisionMode,
        Person preferredExtraneousPayer
    ) {
        repository.deleteByBill_Id(bill.getId());

        List<BillItem> items = bill.getItems();
        List<Person> persons = personService.createPersonsIfNeeded(
            items.stream()
                .flatMap(billItem -> billItem.getPersons().stream())
                .distinct()
                .collect(Collectors.toList())
        );
        
        if (preferredExtraneousPayer == null) {
            // Sorting a random extraneous payer
            // TODO Use a safer RNG?
            List<Person> randomPersons = new ArrayList<>(persons);
            Collections.shuffle(randomPersons);
            preferredExtraneousPayer = randomPersons.get(0);
        }
        
        billItemService.validateCategories(
            items.stream()
                .map(billItem -> billItem.getCategory())
                .collect(Collectors.toList())
        );

        List<BillItem> itemsOfAnyPerson = billItemService.getBillItemsWithAnyPerson(items);
        BigDecimal rawTotalPrice = billItemService.sumBillItems(items);
        BigDecimal itemsOfAnyPersonPrice = billItemService.sumBillItems(itemsOfAnyPerson);

        if (rawTotalPrice.equals(BillUtils.ensurePrecision(BigDecimal.ZERO))) {
            return Arrays.asList();
        }
   
        BillPersonStakeStrategy stakeStrategy = this.getStakeStrategy(bill, divisionMode);

        List<BillPayment> payments = new ArrayList<>();
        BigDecimal summedTotalOfPersons = BillUtils.toTwoDigitsSafe("0.0");
        BigDecimal rawTotalPriceWithPercentages = rawTotalPrice;

        for (Person person : persons) {
            List<BillItem> itemsOfCurrentPerson = billItemService.getBillItemsWithPerson(items, person);
            BigDecimal stake = stakeStrategy.getStake(
                bill,
                person,
                rawTotalPrice,
                itemsOfAnyPerson,
                itemsOfAnyPersonPrice,
                itemsOfCurrentPerson,
                persons
            );

            // Mutliplying percentage items based on stake over total of the current person
            BigDecimal extraPrice = billItemService.applyBillPercentages(
                items,
                person,
                BillUtils.ensurePrecision(rawTotalPrice.multiply(stake)),
                stake
            );
            rawTotalPriceWithPercentages = rawTotalPriceWithPercentages.add(extraPrice);

            for (BillPayment payment : this.generatePaymentsForStake(bill, person, rawTotalPrice, stake, extraPrice)) {
                payments.add(payment);
                summedTotalOfPersons = summedTotalOfPersons.add(payment.getPersonTotal());
            }
        }

        // Finding extraneous payer
        if (!summedTotalOfPersons.equals(rawTotalPriceWithPercentages)) {
            BigDecimal diffExtraneous = rawTotalPriceWithPercentages.subtract(summedTotalOfPersons);
            for (BillPayment payment : payments) {
                if (payment.getPerson().equals(preferredExtraneousPayer)) {
                    payment.setPersonTotal(payment.getPersonTotal().add(diffExtraneous));
                    payment.setExtraneousPayment(diffExtraneous);
                    break;
                }
            }
        }

        repository.saveAll(payments);
        return payments;
    }
    
    public List<BillPayment> generatePaymentsForStake(Bill bill, Person person, BigDecimal rawTotalPrice, BigDecimal stake, BigDecimal extraPrice) {
        BillPayment payment = new BillPayment();
        payment.setBill(bill);
        payment.setPerson(person);
        payment.setBillTotal(rawTotalPrice);

        BigDecimal personPart = BillUtils.ensurePrecision(rawTotalPrice.multiply(stake).add(extraPrice));
        payment.setStake(stake);
        payment.setPersonTotal(personPart);

        return Arrays.asList(payment);
    }

    public BillPersonStakeStrategy getStakeStrategy(Bill bill, String strategy) {
        switch (strategy) {
            case "proportional":
                return new ProportionalBillPersonStakeStrategy();
            case "equality":
                return new EqualityBillPersonStakeStrategy();
            default:
                throw new IllegalArgumentException("Unknown stake division strategy type.");
        }
    }

    private Set<BillPaymentProvider> providers = new HashSet<>();

    public Optional<BillPaymentProvider> getProvider(String name) {
        return this.providers.stream()
            .filter(p -> p.getName().equals(name))
            .findFirst();
    }

    public Map<String, BillPaymentProviderItem> getProvidersForPayment(BillPayment payment) {
        return providers.stream()
            .filter(provider -> provider.canHandle(payment))
            .collect(Collectors.toMap(
                p -> p.getName(),
                p -> new BillPaymentProviderItem(p.getTitle(), p.getFields(payment))
            ));
    }

    public void addProvider(BillPaymentProvider provider) {
        this.providers.add(provider);
    }

}
