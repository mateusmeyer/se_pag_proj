package br.com.mateusmeyer.pag_proj.bill_payment;

public class BillPaymentProviderField {
    private String title;
    private String description;
    private String type;
    private boolean required = true;

    public BillPaymentProviderField() {}

    
    public BillPaymentProviderField(String title, String description, String type) {
        this.title = title;
        this.description = description;
        this.type = type;
    }

    public BillPaymentProviderField(String title, String description, String type, boolean required) {
        this(title, description, type);
        this.required = required;
    }


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
