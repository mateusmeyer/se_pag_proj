package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.Map;

public interface BillPaymentProvider {
    String getName(); 
    String getTitle(); 
    boolean canHandle(BillPayment payment); 
    Map<String, BillPaymentProviderField> getFields(BillPayment payment); 
    Map<String, Map<String, String>> validateFields(BillPayment payment, Map<String, Object> fields);
    String generatePaymentLink(BillPayment payment, Map<String, Object> fields);
}
