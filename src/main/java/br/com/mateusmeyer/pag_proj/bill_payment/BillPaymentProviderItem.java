package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.Map;

public class BillPaymentProviderItem {
    private String title; 
    private Map<String, BillPaymentProviderField> fields;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, BillPaymentProviderField> getFields() {
        return fields;
    }

    public void setFields(Map<String, BillPaymentProviderField> fields) {
        this.fields = fields;
    }

    public BillPaymentProviderItem(String title, Map<String, BillPaymentProviderField> fields) {
        this.title = title;
        this.fields = fields;
    }
}
