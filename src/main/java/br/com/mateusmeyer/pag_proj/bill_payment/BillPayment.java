package br.com.mateusmeyer.pag_proj.bill_payment;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.NaturalId;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillUtils;
import br.com.mateusmeyer.pag_proj.person.Person;

@Entity
public class BillPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Column(length = 16)
    private UUID uniqueId = UUID.randomUUID();

    @ManyToOne
    private Bill bill;
    
    @ManyToOne
    private Person person; 
    private BigDecimal stake;
    private BigDecimal personTotal;
    private BigDecimal billTotal;
    private String preferredPaymentProvider;
    private BigDecimal extraneousPayment = BillUtils.toTwoDigitsSafe("0.0");
    
    @Transient
    private transient Map<String, BillPaymentProviderItem> paymentProviders = Map.of();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Person getPerson() {
        return person;
    }
    
    public void setPerson(Person person) {
        this.person = person;
    }

    public BigDecimal getStake() {
        return stake;
    }

    public void setStake(BigDecimal stake) {
        this.stake = stake;
    }

    public BigDecimal getPersonTotal() {
        return personTotal;
    }

    public void setPersonTotal(BigDecimal stakeableTotal) {
        this.personTotal = stakeableTotal;
    }

    public BigDecimal getBillTotal() {
        return billTotal;
    }

    public void setBillTotal(BigDecimal total) {
        this.billTotal = total;
    }

    public String getPreferredPaymentProvider() {
        return preferredPaymentProvider;
    }

    public void setPreferredPaymentProvider(String paymentProvider) {
        this.preferredPaymentProvider = paymentProvider;
    }

    public BigDecimal getExtraneousPayment() {
        return extraneousPayment;
    }

    public void setExtraneousPayment(BigDecimal extraneousPayment) {
        this.extraneousPayment = extraneousPayment;
    }

    public Map<String, BillPaymentProviderItem> getPaymentProviders() {
        return paymentProviders;
    }

    public void setPaymentProviders(Map<String, BillPaymentProviderItem> paymentProviders) {
        this.paymentProviders = paymentProviders;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BillPayment other = (BillPayment) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null)
                return false;
        } else if (!uniqueId.equals(other.uniqueId))
            return false;
        return true;
    }

    
}
