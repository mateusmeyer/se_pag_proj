package br.com.mateusmeyer.pag_proj.bill_payment;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BillPaymentConfiguration {

    @Resource
    public void configurePaymentProviders(
        BillPaymentService service
    ) {
        service.addProvider(new PicPayLinkBillPaymentProvider());
    }
}
