package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItemService;
import br.com.mateusmeyer.pag_proj.bill.BillService;
import br.com.mateusmeyer.pag_proj.person.Person;

@RestController
public class BillPaymentController {
    @Autowired
    private BillService billService;

    @Autowired
    private BillItemService itemService;

    @Autowired
    private BillPaymentService paymentService;
    
    @PostMapping("/bills/{uid}/payments")
    public ResponseEntity<List<BillPayment>> makePaymentParts(
        @PathVariable("uid") UUID uniqueId,
        @RequestParam(value="extraneousPayer", required=false) UUID extraneousPayer
    ) {
        Bill bill = billService.getByUniqueId(uniqueId).orElseThrow();
        Person extraneousPayerPerson = null;

        if (extraneousPayer != null) {
            extraneousPayerPerson = itemService.getPersonOfBillItems(bill.getItems(), extraneousPayer).orElseThrow();
        }

        List<BillPayment> payments = paymentService.doAndSaveBillDivision(bill, "proportional", extraneousPayerPerson);

        for (BillPayment payment : payments) {
            payment.setPaymentProviders(
                paymentService.getProvidersForPayment(payment)
            );
        }

        return ResponseEntity.ok(payments);
    }

    @PostMapping("/bills/{billUid}/payments/{uid}/link")
    public ResponseEntity<Map<String, String>> makePaymentLink(
        @PathVariable("billUid") UUID billUniqueId,
        @PathVariable("uid") UUID uniqueId,
        @RequestParam("provider") String providerName,
        @RequestBody Map<String, Object> fields
    ) {
        Bill bill = billService.getByUniqueId(billUniqueId).orElseThrow();
        BillPayment payment = paymentService.getByUniqueId(uniqueId).orElseThrow();

        Map<String, BillPaymentProviderItem> validProvidersForPayment = paymentService.getProvidersForPayment(payment);

        if (!validProvidersForPayment.containsKey(providerName)) {
            return ResponseEntity.badRequest().build();
        }

        BillPaymentProvider provider = paymentService.getProvider(providerName).orElseThrow();

        if (provider.canHandle(payment)) {
            Map<String, Map<String, String>> errors = provider.validateFields(payment, fields);
            if (errors.isEmpty()) {
                return ResponseEntity.ok(
                    Map.of("link", provider.generatePaymentLink(payment, fields))
                );
            }
        }

        return ResponseEntity.badRequest().build();
    }
}
