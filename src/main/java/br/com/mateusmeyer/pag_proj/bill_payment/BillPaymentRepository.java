package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.UUID;

import br.com.mateusmeyer.pag_proj.base.NaturalRepository;

public interface BillPaymentRepository extends NaturalRepository<BillPayment, Long, UUID> {
    Long deleteByBill_Id(Long id);
}
