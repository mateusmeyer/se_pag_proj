package br.com.mateusmeyer.pag_proj.bill_payment;

import java.util.HashMap;
import java.util.Map;

/**
 * This BillPaymentProvider generates a simple PicPay payment link (requires the account name).
 */
public class PicPayLinkBillPaymentProvider implements BillPaymentProvider {
    @Override
    public String getName() {
        return "picpay_link";
    }

    @Override
    public String getTitle() {
        return "Link de Pagamento PicPay";
    }

    @Override
    public boolean canHandle(BillPayment payment) {
        return true;
    }

    @Override
    public Map<String, BillPaymentProviderField> getFields(BillPayment payment) {
        return Map.of(
            "target_account", new BillPaymentProviderField(
                "Nome da Conta",
                "O nome da conta do PicPay (o que começa com \"@\".)",
                "string"
            )
        );
    }  

    @Override
    public Map<String, Map<String, String>> validateFields(BillPayment payment, Map<String, Object> fields) {
        boolean hasErrors = false;
        Map<String, Map<String, String>> errors = new HashMap<>();
        errors.put("target_account", new HashMap<>());

        if (
            !fields.containsKey("target_account")
            || (
                fields.get("target_account") instanceof String
                && ((String) fields.get("target_account")).trim().length() == 0
            )
        ) {
            hasErrors = true;
            errors.get("target_account").put("missing_value", "\"Nome da Conta\" não pode estar vazio.");
        }

        if (
            fields.containsKey("target_account")
            && !(fields.get("target_account") instanceof String)
        ) {
            hasErrors = true;
            errors.get("target_account").put("must_be_string", "\"Nome da Conta\" deve ser do tipo textual.");
        }

        return hasErrors
            ? errors
            : Map.of();
    }

    @Override
    public String generatePaymentLink(BillPayment payment, Map<String, Object> fields) {
        String url = "https://picpay.me/";

        url += fields.get("target_account").toString();
        url += "/";
        url += payment.getPersonTotal().toPlainString();

        return url;
    }
}
