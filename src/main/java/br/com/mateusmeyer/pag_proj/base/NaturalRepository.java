package br.com.mateusmeyer.pag_proj.base;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface NaturalRepository<T, ID, NID> extends CrudRepository<T, ID> {
    boolean existsByUniqueId(NID id);
    Optional<T> findByUniqueId(NID id);
}
