package br.com.mateusmeyer.pag_proj.bill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mateusmeyer.pag_proj.person.Person;
import br.com.mateusmeyer.pag_proj.person.PersonService;

@Service
public class BillItemService {
    @Autowired
    private BillItemCategoryRepository itemCategoryRepository;

    @Autowired
    private PersonService personService;

    public List<BillItem> createPersonsIfNeeded(List<BillItem> items) {
        items.forEach(item -> {
            item.setPersons(
                item.getPersons()
                    .stream()
                    .map(person -> {
                        if (person.getId() != null) {
                            return person;
                        } else {
                            Optional<Person> existingPerson = personService.getByUniqueId(person.getUniqueId());

                            if (existingPerson.isPresent()) {
                                return existingPerson.get();
                            }

                            return personService.create(person);
                        }
                    })
                    .distinct()
                    .collect(Collectors.toList())
            );
        });

        return items;
    }

    public List<BillItem> fetchCategoriesIfNeeded(List<BillItem> items) {
        items.forEach(item -> {
            BillItemCategory category = item.getCategory();
            if (category != null) {
                if (category.getId() == null) {
                    category = itemCategoryRepository.findByUniqueId(category.getUniqueId()).orElseThrow();
                
                }
                item.setCategory(category);
            }
        });

        return items;
    }


    public Optional<Person> getPersonOfBillItems(List<BillItem> items, UUID uniqueId) {
        return items.stream()
            .flatMap(item -> item.getPersons().stream())
            .filter(item -> item.getUniqueId().equals(uniqueId))
            .findFirst();
    }

    public List<BillItem> getBillItemsWithoutPerson(List<BillItem> items) {
        return items.stream()
            .filter(item -> item.getPersons() == null || item.getPersons().size() == 0)
            .collect(Collectors.toList());
    }

    public List<BillItem> getBillItemsWithAnyPerson(List<BillItem> items) {
        return getBillItemsWithPerson(items, null);
    }
    
    public List<BillItem> getBillItemsWithPerson(List<BillItem> items, Person person) {
        return items.stream()
            .filter(item -> person != null
                ? (item.getPersons().contains(person))
                : this.hasPersonsInBillItem(item))
            .collect(Collectors.toList());
    }

    public boolean hasPersonsInBillItem(BillItem item) {
        return item.getPersons() != null && item.getPersons().size() > 0;
    }

    public BigDecimal sumBillItems(List<BillItem> items) {
        return BillUtils.ensurePrecision(
            items.stream()
                .filter(item -> !item.isPercent())
                .map(this::getPriceByCategory)
                .reduce(new BigDecimal(0), BigDecimal::add)
        );
    }

    public BigDecimal applyBillPercentages(List<BillItem> items, Person person, BigDecimal total, BigDecimal stake) {
        return BillUtils.ensurePrecision(
            items.stream()
                .filter(item -> item.isPercent() && (!hasPersonsInBillItem(item) || item.getPersons().contains(person)))
                .map(item -> hasPersonsInBillItem(item)
                    ? this.applyPercentageByCategoryAndPerson(item, total)
                    : this.applyPercentageByCategoryAndStake(item, total, stake))
                .reduce(new BigDecimal(0), BigDecimal::add)
        );
    }

    protected BigDecimal getPriceByCategory(BillItem item) {
        BigDecimal price = item.getPrice().abs();

        if (item.getCategory().isDiscount()) {
            price = price.negate();
        }

        return price;
    }

    protected BigDecimal applyPercentageByCategoryAndPerson(BillItem item, BigDecimal partTotal) {
        BigDecimal percentual = item.getPrice().abs();

        if (item.getCategory().isDiscount()) {
            percentual = percentual.negate();
        }

        // Dividing percentuals between 1+ persons is a equality task
        percentual = percentual.divide(new BigDecimal(100)).setScale(4, RoundingMode.HALF_UP);
        percentual = percentual.divide(new BigDecimal(item.getPersons().size())).setScale(4, RoundingMode.HALF_UP);
        return BillUtils.ensurePrecision(percentual.multiply(partTotal));
    }

    protected BigDecimal applyPercentageByCategoryAndStake(BillItem item, BigDecimal partTotal, BigDecimal stake) {
        BigDecimal percentual = item.getPrice().abs();

        if (item.getCategory().isDiscount()) {
            percentual = percentual.negate();
        }

        percentual = percentual.divide(new BigDecimal(100)).setScale(4, RoundingMode.HALF_UP);
        percentual = percentual.multiply(stake).setScale(4, RoundingMode.HALF_UP);
        return BillUtils.ensurePrecision(percentual.multiply(partTotal));
    }

    public List<BillItemCategory> validateCategories(List<BillItemCategory> categories) {
        for (BillItemCategory category : categories) {
            if (category != null) {
                if (!itemCategoryRepository.existsByUniqueId(category.getUniqueId())) {
                    throw new IllegalArgumentException("Categories must exist.");
                }
            }
        }

        return categories;
    }

    public List<BillItem> validateBillItems(List<BillItem> items) {
        return items.stream()
            .map(this::validateBillItem)
            .collect(Collectors.toList());
    }

    public BillItem validateBillItem(BillItem item) {
        if (item.getCategory().isPersonNeeded() && (item.getPersons() == null || item.getPersons().isEmpty())) {
            throw new IllegalArgumentException("This category of item needs at least one person associated.");
        }

        return item;
    }
}
