package br.com.mateusmeyer.pag_proj.bill;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BillUtils {
    public static BigDecimal toTwoDigitsSafe(int input) {
        return toTwoDigitsSafe(Integer.toString(input));
    }

    public static BigDecimal toTwoDigitsSafe(String input) {
        return ensurePrecision(new BigDecimal(input));
    } 

    public static BigDecimal toFourDigitsSafe(String input) {
        return ensurePrecision(new BigDecimal(input), 4);
    }

    public static BigDecimal ensurePrecision(BigDecimal decimal) {
        return ensurePrecision(decimal, 2);
    }

    public static BigDecimal ensurePrecision(BigDecimal decimal, int scale) {
        return decimal
            .setScale(scale, RoundingMode.HALF_EVEN);
    } 
}
