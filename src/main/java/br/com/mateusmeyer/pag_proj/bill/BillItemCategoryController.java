package br.com.mateusmeyer.pag_proj.bill;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bill-item-categories")
public class BillItemCategoryController {
    @Autowired
    protected BillItemCategoryRepository repository;

    @GetMapping
    public ResponseEntity<Iterable<BillItemCategory>> getCategories() {
        return new ResponseEntity<Iterable<BillItemCategory>>(repository.findAll(), HttpStatus.OK);
    }
}
