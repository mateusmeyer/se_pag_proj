package br.com.mateusmeyer.pag_proj.bill;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.mateusmeyer.pag_proj.person.Person;

@Entity
public class BillItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @PrimaryKeyJoinColumn
    @NotNull
    @JsonIgnore
    private Bill bill;

    @Column(nullable = false)
    @NotEmpty
    private String name;

    @NotNull
    @Digits(integer=10, fraction = 2)
    @Column(precision = 10, scale = 2, nullable=false)
    private BigDecimal price;

    private boolean percent = false;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @NotNull
    private List<Person> persons = new ArrayList<>();

    @ManyToOne
    private BillItemCategory category;

    @NaturalId
    @Column(length = 16)
    private UUID uniqueId = UUID.randomUUID();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isPercent() {
        return percent;
    }

    public void setPercent(boolean percent) {
        this.percent = percent;
    }

    public BillItemCategory getCategory() {
        return category;
    }

    public void setCategory(BillItemCategory category) {
        this.category = category;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }
 

    public BillItem() {
    }

    public BillItem(@NotNull Bill bill, @NotEmpty String name,
            @NotNull @Digits(integer = 10, fraction = 2) BigDecimal price, @NotNull List<Person> persons,
            BillItemCategory category) {
        this(name, price, persons, category);
        this.bill = bill;
    }

    public BillItem(@NotNull Bill bill, @NotEmpty String name,
            @NotNull @Digits(integer = 10, fraction = 2) BigDecimal price, @NotNull List<Person> persons,
            BillItemCategory category, UUID uniqueId) {
        this(bill, name, price, persons, category);
        this.uniqueId = uniqueId;
    }

    public BillItem(@NotEmpty String name, @NotNull @Digits(integer = 10, fraction = 2) BigDecimal price,
            @NotNull List<Person> persons, BillItemCategory category) {
        this.name = name;
        this.price = price;
        this.persons = persons;
        this.category = category;
    }

    public BillItem(@NotEmpty String name, @NotNull @Digits(integer = 10, fraction = 2) BigDecimal price,
            @NotNull List<Person> persons, BillItemCategory category, boolean percent) {
        this(name, price, persons, category);
        this.percent = percent;
    }

    public BillItem(@NotEmpty String name, @NotNull @Digits(integer = 10, fraction = 2) BigDecimal price,
            @NotNull List<Person> persons, BillItemCategory category, UUID uniqueId) {
        this(name, price, persons, category);
        this.uniqueId = uniqueId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BillItem other = (BillItem) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null)
                return false;
        } else if (!uniqueId.equals(other.uniqueId))
            return false;
        return true;
    }

    
    
}
