package br.com.mateusmeyer.pag_proj.bill;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillService {
    @Autowired
    private BillItemService billItemService;

    @Autowired
    private BillRepository repository;

    public Optional<Bill> getByUniqueId(UUID uniqueId) {
        return repository.findByUniqueId(uniqueId);
    }

    @Transactional
    public Bill create(Bill item) {
        if (item.getId() != null) {
            throw new IllegalArgumentException("Cannot create a item with id.");
        }

        billItemService.createPersonsIfNeeded(item.getItems());
        billItemService.fetchCategoriesIfNeeded(item.getItems());
        billItemService.validateBillItems(item.getItems());
        return repository.save(item);
    }

    @Transactional
    public Bill update(Bill item) {
        billItemService.createPersonsIfNeeded(item.getItems());
        billItemService.fetchCategoriesIfNeeded(item.getItems());
        return repository.save(item);
    }
}
