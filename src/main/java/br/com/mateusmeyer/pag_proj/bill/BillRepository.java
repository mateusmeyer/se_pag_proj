package br.com.mateusmeyer.pag_proj.bill;

import java.util.UUID;

import br.com.mateusmeyer.pag_proj.base.NaturalRepository;

public interface BillRepository extends NaturalRepository<Bill, Long, UUID> {
    
}
