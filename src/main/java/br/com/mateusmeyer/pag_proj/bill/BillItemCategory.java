package br.com.mateusmeyer.pag_proj.bill;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.NaturalId;

@Entity
public class BillItemCategory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @NaturalId
    @Column(length = 16)
    private UUID uniqueId = UUID.randomUUID();

    private boolean discount = false;

    private boolean personNeeded = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public BillItemCategory() {
        
    }    

    public BillItemCategory(String name) {
        this.name = name;
    }

    public BillItemCategory(String name, boolean discount) {
        this(name);
        this.discount = discount;
    }

    public BillItemCategory(String name, boolean discount, boolean personNeeded) {
        this(name, discount);
        this.personNeeded = personNeeded;
    }

    public BillItemCategory(String name, UUID uniqueId) {
        this(name);
        this.uniqueId = uniqueId;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public boolean isPersonNeeded() {
        return personNeeded;
    }

    public void setPersonNeeded(boolean personNeeded) {
        this.personNeeded = personNeeded;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BillItemCategory other = (BillItemCategory) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null)
                return false;
        } else if (!uniqueId.equals(other.uniqueId))
            return false;
        return true;
    }
    
}
