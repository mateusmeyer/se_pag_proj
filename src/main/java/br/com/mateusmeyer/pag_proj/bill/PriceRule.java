package br.com.mateusmeyer.pag_proj.bill;

import java.util.List;

import br.com.mateusmeyer.pag_proj.person.Person;

public interface PriceRule {
    Double getPrice(
        BillItem item,
        Bill bill,
        Double rawTotal,
        Double rawPersonTotal,
        List<Person> participants,
        List<BillItem> currentPersonItems
    );
}
