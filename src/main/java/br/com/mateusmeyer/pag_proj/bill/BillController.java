package br.com.mateusmeyer.pag_proj.bill;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bills")
public class BillController {
    @Autowired
    private BillService service;

    @PostMapping
    public ResponseEntity<Bill> createBill(@RequestBody @Valid Bill bill) {
        for (BillItem billItem : bill.getItems()) {
            billItem.setBill(bill);
        }
        bill = service.create(bill);
        return ResponseEntity.ok(bill);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Bill> updateBill(
        @PathVariable("id") Long uniqueId,
        @RequestBody @Valid Bill bill
    ) {
        bill.setId(uniqueId);

        for (BillItem billItem : bill.getItems()) {
            billItem.setBill(bill);
        }
        bill = service.update(bill);
        return ResponseEntity.ok(bill);
    }
}
