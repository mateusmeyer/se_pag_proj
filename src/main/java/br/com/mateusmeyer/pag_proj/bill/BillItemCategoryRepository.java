package br.com.mateusmeyer.pag_proj.bill;

import java.util.UUID;

import br.com.mateusmeyer.pag_proj.base.NaturalRepository;

public interface BillItemCategoryRepository extends NaturalRepository<BillItemCategory, Long, UUID> {
    
}
