package br.com.mateusmeyer.pag_proj.person;

import java.util.UUID;

import br.com.mateusmeyer.pag_proj.base.NaturalRepository;

public interface PersonRepository extends NaturalRepository<Person, Long, UUID> {
    
}
