package br.com.mateusmeyer.pag_proj.person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    @Autowired
    private PersonRepository repository;

    public Optional<Person> getByUniqueId(UUID uniqueId) {
        return repository.findByUniqueId(uniqueId);
    }


    public Person create(Person person) {
        if (person.getId() != null) {
            throw new IllegalArgumentException("Cannot create a item with id.");
        }

        return repository.save(person);
    }


    public List<Person> createPersonsIfNeeded(List<Person> persons) {
        return persons.stream()
            .distinct()
            .map(repository::save)
            .collect(Collectors.toList());
    }
}
