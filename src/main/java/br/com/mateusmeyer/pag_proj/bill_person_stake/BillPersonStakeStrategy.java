package br.com.mateusmeyer.pag_proj.bill_person_stake;

import java.math.BigDecimal;
import java.util.List;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.person.Person;

/**
 * Person Stake is the ratio of what each person will pay
 * on non-person-associated items.
 * 
 * - A tip or any debit or discount with person=null will be splitted by all persons by using the
 *   ratio returned by BillPersonStake (which is applied to every participating person)
 */
public interface BillPersonStakeStrategy {
    BigDecimal getStake(
        Bill bill,
        Person person,
        BigDecimal rawTotalPrice,
        List<BillItem> itemsOfAnyPerson,
        BigDecimal itemsOfAnyPersonPrice,
        List<BillItem> itemsOfCurrentPerson,
        List<Person> totalParticipants
    );
}
