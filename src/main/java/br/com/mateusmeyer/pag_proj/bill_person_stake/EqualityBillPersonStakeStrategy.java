package br.com.mateusmeyer.pag_proj.bill_person_stake;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.bill.BillUtils;
import br.com.mateusmeyer.pag_proj.person.Person;

/**
 * Calculates the item final price based by equity (every person pays the same amount, regardless of items value)
 */
public class EqualityBillPersonStakeStrategy implements BillPersonStakeStrategy {
    @Override
    public BigDecimal getStake(
        Bill bill,
        Person person,
        BigDecimal rawTotalPrice,
        List<BillItem> itemsOfAnyPerson,
        BigDecimal itemsOfAnyPersonPrice,
        List<BillItem> itemsOfCurrentPerson,
        List<Person> totalParticipants
    ) {
        return BillUtils.toTwoDigitsSafe("1")
            .divide(
                BillUtils.toTwoDigitsSafe(totalParticipants.size()), 4, RoundingMode.HALF_EVEN);
    }
}
