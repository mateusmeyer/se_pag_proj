package br.com.mateusmeyer.pag_proj.bill_person_stake;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import br.com.mateusmeyer.pag_proj.bill.Bill;
import br.com.mateusmeyer.pag_proj.bill.BillItem;
import br.com.mateusmeyer.pag_proj.person.Person;

/**
 * Calculates the item final price based on the stake (price of bill items associated)
 * the person has on the bill.
 */
public class ProportionalBillPersonStakeStrategy implements BillPersonStakeStrategy {
    @Override
    public BigDecimal getStake(
        Bill bill,
        Person person,
        BigDecimal rawTotalPrice,
        List<BillItem> itemsOfAnyPerson,
        BigDecimal itemsOfAnyPersonPrice,
        List<BillItem> itemsOfCurrentPerson,
        List<Person> totalParticipants
    ) {
        // Stake is calculated based on ratio of person spended value in relation of all persons spended value
        // (minus shared items like tip or relation)
        BigDecimal itemsOfCurrentPersonPrice = itemsOfCurrentPerson.stream()
            .map(item -> item.getPersons().size() > 1
                ? this.mapItemMultiplePersons(item)
                : item.getPrice())
            .reduce(new BigDecimal(0), BigDecimal::add);
        return itemsOfCurrentPersonPrice.divide(itemsOfAnyPersonPrice, 4, RoundingMode.HALF_EVEN);
    }

    protected BigDecimal mapItemMultiplePersons(BillItem item) {
        return item.getPrice().divide(new BigDecimal(item.getPersons().size()), 4, RoundingMode.HALF_EVEN);
    }
}