# pag_proj
Servidor Web para desafio da SE. É executado por padrão, no ambiente de desenvolvimento, na porta 8083.

## Requisitos
- Java 8-20 (o _target_ do projeto é o Java 8)
- Gradle 7 ou superior

## Como executar (desenvolvimento)
- `gradle bootRun`

## Detalhes
- Efetua o sorteio automático do dinheiro faltante ou sobrante por padrão caso exista uma divisão inexata (como 100/3);
- Permite a utilização de estratégias de divisão de cotas (stakes) para arcar com os custos gerais; a padrão é a divisão proporcional (conforme gasto pela pessoa);
- Embora possa ser executado com o MySQL, para facilitar os testes, foi configurado para utilizar o banco H2 (em memória, ou seja, é esvaziado toda vez que o programa é encerrado). Pode ser ajustado para utilizar o MySQL nas configurações do Spring.
- Sempre que algum item é atribuído a mais de uma pessoa, ele será dividido igualitariamente entre elas